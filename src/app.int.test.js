/**
 * @jest-environment jsdom
 */

const { makeOperation, makeHistory } = require('./app');

describe('makeOperation', () => {
    test('it should make operations', () => {
        
        const inputs = [
            { operation: 'add', result: 30, a: 25, b: 5 },
            { operation: 'sub', result: 10, a: 15, b: 5 },
            { operation: 'add', result: 2, a: -2,   b: 4 },
        ];
        
        for (const {operation, result, a, b} of inputs) {
            expect(makeOperation(operation, a, b)).toBe(result);
        }
        
    });
    test('it should not make operations', () => {
        
        const inputs = [
            { operation: '+',  a: 25, b: 5 },
            { operation: '-',  a: 15, b: 5 },
            { operation: 'add', b: 5 },
            { a: -2,   b: 4 },
        ];
        
        for (const {operation, a, b} of inputs) {
            expect(makeOperation(operation, a, b)).toBe(undefined);
        }
        
    });
});

describe('makeHistory', () => {
    test('it should return an history element', () => {
        
        const inputs = [
            { operation: 'add', result: 30, a: 25, b: 5 },
            { operation: 'sub', result: 10, a: 15, b: 5 },
            { operation: 'add', result: 2, a: -2,   b: 4 },
        ];
        
        for (const { operation, result, a, b } of inputs) {
            expect(makeHistory(operation, result, a, b).innerHTML).toBe(`${operation}: ${a} and ${b} = ${result}`);
        }
        
    });
    test('it should not return an history element with undefined operation', () => {
        
        const inputs = [
            { a: 25, b: 5 },
            { b: 5 },
        ];
        
        for (const {operation, result, a, b} of inputs) {
            expect(makeHistory(operation, result, a, b)).toBe(undefined);
        }
        
    });

    test('it should return an history element with undefined elements', () => {
        
        const inputs = [
            { operation: '+', b: 5 },
            { operation: '-',  a: 15 },
            { operation: 'add', b: 5 },
        ];
        
        for (const {operation, result, a, b} of inputs) {
            expect(makeHistory(operation, result, a, b).innerHTML).toBe(`${operation}: ${a} and ${b} = ${result}`);
        }
        
    });
});