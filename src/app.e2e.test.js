const puppeteer = require('puppeteer');

describe('End-to-end test', () => {
    test('should add a new operation', async () => {
        const browser = await puppeteer.launch({ headless: true });
        const page = await browser.newPage();
        await page.setDefaultNavigationTimeout(0);
        await page.goto('file:///Users/flavia.matos/Documents/academy/database/test-essentials-assignment/index.html');
        await page.type('input#operation', 'add');
        await page.type('input#first-value', '25');
        await page.type('input#second-value', '3');
        await page.click('#submit');
        const finalText = await page.$eval('.history-item', element => element.textContent);
        expect(finalText).toBe('add: 25 and 3 = 28');
    });
    test('should not add hisitory when input is empty', async () => {
        const browser = await puppeteer.launch({ headless: true });
        const page = await browser.newPage();
        await page.goto('file:///Users/flavia.matos/Documents/academy/database/test-essentials-assignment/index.html');

        await page.click('#submit');
        let childElementCount = await page.$eval('.history-list', element => element.childElementCount);
        expect(childElementCount).toBe(0);

        await page.type('input#first-value', '25');
        await page.click('#submit');
        childElementCount = await page.$eval('.history-list', element => element.childElementCount);
        expect(childElementCount).toBe(0);
    }, 1000000);

    test('should keep history', async () => {
        const browser = await puppeteer.launch({ headless: true });
        const page = await browser.newPage();
        await page.goto('file:///Users/flavia.matos/Documents/academy/database/test-essentials-assignment/index.html');

        await page.type('input#operation', 'add');
        await page.type('input#first-value', '25');
        await page.type('input#second-value', '3');
        await page.click('#submit');
        await page.type('input#operation', 'add');
        await page.type('input#first-value', '2');
        await page.type('input#second-value', '3');
        await page.click('#submit');
        await page.type('input#operation', 'sub');
        await page.type('input#first-value', '4');
        await page.type('input#second-value', '3');
        await page.click('#submit');
        let childElementCount = await page.$eval('.history-list', element => element.childElementCount);
        expect(childElementCount).toBe(3);
    }, 1000000);
});