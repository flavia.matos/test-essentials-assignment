const { createElement, checkOperation, add, sub, generateText } = require('./util');

const initApp = () => {
    const button = document.querySelector('#submit');
    if (button) {
        button.addEventListener('click', onClick);   
    }
};

const onClick = () => {
    const operation = document.querySelector('input#operation').value.trim();
    const a = document.querySelector('input#first-value').value.trim();
    const b = document.querySelector('input#second-value').value.trim();
    const historyList = document.querySelector('.history-list');

    const result = makeOperation(operation, a, b);
    const history = makeHistory(operation, result, a, b);
    if (history) {
        historyList.appendChild(history);
    }
}

const makeOperation = (operation, a, b) => {
    const isValidOperation = checkOperation(operation);
    if (!isValidOperation || !a || !b) {
        return;
    }
    const operationsFunction = {
        'add': add,
        'sub': sub
    }
    return operationsFunction[operation](+a,+b);
}

const makeHistory = (operation, result, a, b) => {
    if(!operation) {
        return;
    }
    const outputText = generateText(operation, result, a, b);
    if (!outputText) {
        return;
    }
    return createElement('li', outputText, 'history-item');
};

initApp();

module.exports = { makeOperation, makeHistory };