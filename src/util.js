exports.generateText = (operation, result, a, b) => {
    return `${operation}: ${a} and ${b} = ${result}`;
};

exports.createElement = (type, text, className) => {
    const newElement = document.createElement(type);
    newElement.classList.add(className);
    newElement.textContent = text;
    return newElement;
};

exports.checkOperation = (operation) => {
    if (!operation) {
        return false;
    }
    if (operation !== 'add' && operation !== 'sub') {
        return false;
    }
    return true;
}

exports.add = (a,b) => a+b;
exports.sub = (a,b) => a-b;
