/**
 * @jest-environment jsdom
 */

const { generateText, createElement, checkOperation, add, sub } = require('./util');

describe('generateText', () => {
    test('it should generate text with input', () => {
        const inputs = [
            { operation: 'add', result: 30, a: 25, b: 5 },
            { operation: 'sub', result: 10, a: 15, b: 5 },
            { operation: 'add', a: -2, result: 2,  b: 4 },
        ];
        
        for (const {op, result, a, b} of inputs) {
            expect(generateText(op, result, a, b)).toBe(`${op}: ${a} and ${b} = ${result}`);
        }
        
    });
    test('it should generate text without input', () => {
        expect(generateText()).toBe('undefined: undefined and undefined = undefined');
    });
});

describe('checkOperation', () => {
    test('it should return the operation is valid', () => {
        const operations = ['add', 'sub'];
        
        for (const operation of operations) {
            expect(checkOperation(operation)).toBe(true);
        }
        
    });
    test('it should return the operation is not valid', () => {
        const operations = ['+','-', 'subtract', ''];
        for (const operation of operations) {
            expect(checkOperation(operation)).toBe(false);
        }
    });
});

describe('createElement', () => {
    test('it should create an element of type a', () => {
        const type = 'a';
        const text = 'this is an example';
        const className = 'link';
        const element = createElement(type, text, className);
        expect(element).not.toBeNull();
        expect(element.text).toBe('this is an example');
        expect(element.className).toBe('link');
    });

    test('it should create an element of type li without class', () => {
        const type = 'li';
        const text = 'this is an example';
        const element = createElement(type, text);
        expect(element).not.toBeNull();
        expect(element.innerHTML).toBe('this is an example');
        expect(element.className).toBe('undefined');
    });
});

describe('add', () => {
    test('it should add two numbers', () => {
        expect(add(1,2)).toBe(3);
        expect(add(-4,2)).toBe(-2);
        expect(add(0,0)).toBe(0);
    });
});

describe('sub', () => {
    test('it should subtract two numbers', () => {
        expect(sub(1,2)).toBe(-1);
        expect(sub(-4,2)).toBe(-6);
        expect(sub(0,0)).toBe(0);
    });
});